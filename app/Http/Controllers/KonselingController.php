<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Konseling;
use App\Kategori;
use Auth;

class KonselingController extends Controller
{
    protected $konseling;

    public function __construct()
    {
        $this->konseling = new Konseling();
    }

    public function index()
    {
        return view('admin.konseling.index', ['konseling' => $this->konseling->orderBy('waktu', 'desc')->get() ]);
    }

    public function response($id)
    {
        if(Auth::guard('web')->check()){
            $data['konseling'] = $this->konseling->find($id);
            return view('admin.konseling.show', $data);
        } else {
            $data['konseling'] = $this->konseling->where('id', $id)->first();
            if($data['konseling']->status == '0'){
                if(Auth::guard('mahasiswa')->user()->id != $data['konseling']->mahasiswa_id){
                    return redirect()->back()->with('error', 'Tidak dapat mengakses konseling tersebut');
                }
            }
            return view('mahasiswa.listing.detail_konseling', $data);
        }
    }

    public function create()
    {
        $data['kategori'] = Kategori::orderBy('nama_kategori', 'asc')->get();
        return view('mahasiswa.form_konseling', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul_konseling' => 'required',
            'isi' => 'required',
            'kategori_id' => 'required',
            'status' => 'required'
        ]);
        $request['waktu'] = date('Y-m-d H:i:s');
        $request['mahasiswa_id'] = Auth::guard('mahasiswa')->user()->id;
        Konseling::create($request->all());

        return redirect()->back()->with('success', 'Konseling anda berhasil dibuat, konselor akan segera mengecek dan memberi respon, terima kasih');
    }

}
